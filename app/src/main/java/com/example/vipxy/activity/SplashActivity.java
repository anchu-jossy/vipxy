package com.example.vipxy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.vipxy.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.btn_getstarted)
    Button btnGetstarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(SplashActivity.this);


    }


    @OnClick(R.id.btn_getstarted)
    public void onViewClicked() {
        startActivity(new Intent(SplashActivity.this,HomeActivity.class));
    }
}
