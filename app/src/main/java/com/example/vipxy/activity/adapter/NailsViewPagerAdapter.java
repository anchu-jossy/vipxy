package com.example.vipxy.activity.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class NailsViewPagerAdapter extends FragmentPagerAdapter {
    private final List mFragmentList = new ArrayList<>();
    private final List mFragmentTitleList = new ArrayList<>();
    ArrayList<String> fragmentArray = new ArrayList<>();

    public NailsViewPagerAdapter(FragmentManager manager, ArrayList<String> fragmentArray) {
        super(manager);
        this.fragmentArray = fragmentArray;
    }


    @Override
    public Fragment getItem(int position) {
        return (Fragment) mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }
    public void addFragOnSearch(Fragment fragment) {
        mFragmentList.add(fragment);

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return (CharSequence) mFragmentTitleList.get(position);
    }
}