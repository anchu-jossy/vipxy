package com.example.vipxy.activity.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.vipxy.R;
import com.example.vipxy.activity.fragments.ManicureFragment;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ManicureAdapter extends RecyclerView.Adapter<ManicureAdapter.MyViewHolder> {

    ImageView imageView;
    ArrayList<Integer> imageArray = new ArrayList<>();
    ArrayList<String> headingArray = new ArrayList<>();
    ArrayList<String> priceArray = new ArrayList<>();
    ArrayList<String> detailArray = new ArrayList<>();
    ManicureFragment context;


    public ManicureAdapter(ManicureFragment fragment, ArrayList<Integer> imageArray, ArrayList<String> headingArray,
                           ArrayList<String> priceArray, ArrayList<String> detailArray) {
        this.context = fragment;
        this.imageArray = imageArray;
        this.headingArray = headingArray;
        this.priceArray = priceArray;
        this.detailArray = detailArray;
        Log.d("ManicureAdapter123", imageArray.size() + " " + headingArray.size() + "  " + priceArray + "  " + detailArray.size() + " ");

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_manicure_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(imageArray.get(position)).into(holder.imageview);
        holder.tvHeading.setText(headingArray.get(position));
        holder.tvPrice.setText(priceArray.get(position));
        holder.tvHeading.setText(headingArray.get(position));
        holder.tvDetail.setText(detailArray.get(position));
        Log.d("Manicuredapter123", "onBindViewHolder: "+detailArray.get(position));

    }


    @Override
    public int getItemCount() {
        return imageArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageview)
        ImageView imageview;
        @BindView(R.id.tv_heading)
        TextView tvHeading;
        @BindView(R.id.tv_price)
        TextView tvPrice;
        @BindView(R.id.tv_detail)
        TextView tvDetail;
        @BindView(R.id.ll_beauty_detail)
        LinearLayout llBeautyDetail;
        @BindView(R.id.separator)
        View separator;
        @BindView(R.id.card)
        LinearLayout card;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }
}
