package com.example.vipxy.activity.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.vipxy.R;
import com.example.vipxy.activity.MakeupHair_Beauty_SubActivity;
import com.example.vipxy.activity.MassageHealthActivity;
import com.example.vipxy.activity.dialogfragments.OtpVerificationDialogFragment;
import com.example.vipxy.activity.dialogfragments.PackageSelectionDialogFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MassageHealthAdapter extends RecyclerView.Adapter<MassageHealthAdapter.MyViewHolder> {

    ImageView imageView;
    ArrayList<Integer> imageArray = new ArrayList<>();
    ArrayList<String> headingArray = new ArrayList<>();
    ArrayList<String> price1Array = new ArrayList<>();
    ArrayList<String> price2Array = new ArrayList<>();
    ArrayList<String> price3Array = new ArrayList<>();
    MassageHealthActivity context;


    public MassageHealthAdapter(MassageHealthActivity context, ArrayList<Integer> imageArray, ArrayList<String> headingArray, ArrayList<String> price1Array,
                                ArrayList<String> price2Array, ArrayList<String> price3Array) {
        this.context = context;
        this.imageArray = imageArray;
        this.headingArray = headingArray;
        this.price1Array = price1Array;
        this.price2Array = price2Array;
        this.price3Array=price3Array;
        Log.d("ManicureAdapter123", imageArray.size() + " " + headingArray.size() + "  " + price1Array + "  " + price2Array.size() + " "+price3Array.size()+"");

    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_massage_health, parent, false);
        ButterKnife.bind(this, itemView);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(imageArray.get(position)).into(holder.imageview);
        holder.tvHeading.setText(headingArray.get(position));
        holder.tvPrice1.setText(price1Array.get(position));
        holder.tvHeading.setText(headingArray.get(position));
        holder.tvPrice2.setText(price2Array.get(position));
        holder.tvPrice3.setText(price3Array.get(position));
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "click", Toast.LENGTH_SHORT).show();
              showSelectionPackageDialog();
            }
        });


    }
    private void showSelectionPackageDialog() {
        PackageSelectionDialogFragment dialogFragment = new PackageSelectionDialogFragment();
        FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
        Fragment prev = ((AppCompatActivity)context).getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        dialogFragment.show(ft, "dialog");
    }

    @Override
    public int getItemCount() {
        return imageArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageview)
        ImageView imageview;
        @BindView(R.id.tv_heading)
        TextView tvHeading;
        @BindView(R.id.tv_price1)
        TextView tvPrice1;
        @BindView(R.id.tv_price2)
        TextView tvPrice2;
        @BindView(R.id.tv_price3)
        TextView tvPrice3;
        @BindView(R.id.ll_beauty_detail)
        LinearLayout llBeautyDetail;
        @BindView(R.id.separator)
        View separator;
        @BindView(R.id.card)
        LinearLayout card;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }
}
