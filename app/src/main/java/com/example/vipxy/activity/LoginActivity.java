package com.example.vipxy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.vipxy.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements DialogFragment.DialogListener {

    @BindView(R.id.btn_get_otp)
    Button btnGetOtp;
    @BindView(R.id.btn_getlogin)
    Button btnGetlogin;
    @BindView(R.id.tv_forgot_password)
    TextView tvForgot_password;
    @BindView(R.id.scrollView)
    ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);



    }

    private void showOtpDialog() {
        DialogFragment dialogFragment = new DialogFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        dialogFragment.show(ft, "dialog");
    }

    @Override
    public void onFinishEditDialog(String inputText) {

    }

    @OnClick({R.id.btn_get_otp, R.id.btn_getlogin, R.id.scrollView})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_get_otp:
                showOtpDialog();
                break;
            case R.id.btn_getlogin:
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                break;
            case R.id.scrollView:

                break;
        }
    }
}
