package com.example.vipxy.activity;

import android.app.SearchManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vipxy.R;
import com.example.vipxy.activity.adapter.MakeupHairBeautySubActivityAdapter;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MakeupHair_Beauty_SubActivity extends AppCompatActivity {

    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.bottomsheet)
    LinearLayout bottomsheet;
    BottomSheetBehavior mBottomSheetBehavior;
    SearchView searchView;
    ArrayList<Integer> imageArray = new ArrayList<>();
    ArrayList<String> headingArray = new ArrayList<>();
    ArrayList<String> priceArray = new ArrayList<>();
    ArrayList<String> detailArray = new ArrayList<>();
    @BindView(R.id.imageview_back)
    ImageView imageviewBack;
    @BindView(R.id.imageview_vipxy)
    ImageView imageviewVipxy;

    LinearLayout llToolbar;
    private String TAG = "MakeupHair_Beauty_SubActivity123";
    MakeupHairBeautySubActivityAdapter makeupHairBeautySubActivityAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hairmakeup_beauty__sub);
        ButterKnife.bind(this);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomsheet);
        bottomsheet.setMinimumHeight(1);
        tvToolbarTitle.setText(R.string.makeup_hair);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        setHairAndMakeupListAdapter();
    }


    private void setHairAndMakeupListAdapter() {
        if (imageArray != null) {
            imageArray.clear();
            imageArray.add(R.drawable.bespoke_blow_dry);
            imageArray.add(R.drawable.up_do_hair);
            imageArray.add(R.drawable.bespoke_makeup);
            imageArray.add(R.drawable.bespoke_hair_makeup);
        }

        if (headingArray != null) {
            headingArray.clear();
            headingArray.add("BESPOKE BLOW DRY");
            headingArray.add("UP DO HAIR STYLING");
            headingArray.add("BESPOKE MAKEUP");
            headingArray.add("BESPOKE HAIR AND MAKEUP PACKAGE");
        }
        if (priceArray != null) {

            priceArray.add("$99 for 45 minutes");
            priceArray.add("$120 for 60 minutes");
            priceArray.add("$135 for 60 minutes");
            priceArray.add("$199 for 120 minutes");
        }
        if (detailArray != null) {
            detailArray.add("Choose from a sleek bouncy or voluminous blow dry,styled by our amazing hair stylists");
            detailArray.add("Choose an up-do of your choice and we'll ensure your hair is read carpet ready.");
            detailArray.add("Ensure your look flawless for your big event or special night out");
            detailArray.add("Our skilled hair & makeup artists will provide a bespoke makeup look of your choice,along with a bespoke blow dry or up-do");
        }


        makeupHairBeautySubActivityAdapter = new MakeupHairBeautySubActivityAdapter(this, imageArray, headingArray, priceArray, detailArray);
        recyclerview.setLayoutManager(new LinearLayoutManager(MakeupHair_Beauty_SubActivity.this));
        recyclerview.setHasFixedSize(false);
        recyclerview.setAdapter(makeupHairBeautySubActivityAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d(TAG, "onQueryTextSubmit: " + s);
                String newText = s.toLowerCase();

                for (int i = 0; i < headingArray.size(); i++) {
                    Log.d(TAG, "onQueryTextSubmit: " + newText);

                    if (Pattern.compile(Pattern.quote(headingArray.get(i)), Pattern.CASE_INSENSITIVE).matcher(newText).find()) {
                        Log.d(TAG, "onQueryTextSubmit: " + newText + headingArray.get(i));
                        ArrayList<String> heading1Array = new ArrayList<>();
                        heading1Array.add(headingArray.get(i));
                        ArrayList<String> price1Array = new ArrayList<>();
                        price1Array.add(priceArray.get(i));
                        ArrayList<Integer> image1Array = new ArrayList<>();
                        image1Array.add(imageArray.get(i));
                        ArrayList<String> detail1Array = new ArrayList<>();
                        detail1Array.add(detailArray.get(i));
                        makeupHairBeautySubActivityAdapter.notifyDataSetChanged();

                        makeupHairBeautySubActivityAdapter = new MakeupHairBeautySubActivityAdapter(MakeupHair_Beauty_SubActivity.this, image1Array, heading1Array, price1Array, detail1Array);
                        recyclerview.setLayoutManager(new LinearLayoutManager(MakeupHair_Beauty_SubActivity.this));
                        recyclerview.setHasFixedSize(false);
                        recyclerview.setAdapter(makeupHairBeautySubActivityAdapter);
                        makeupHairBeautySubActivityAdapter.notifyDataSetChanged();
                    } else {
                        Log.d(TAG, "onQueryTextSubmit:nomatch ");
                    }


                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {


                return true;

            }

        });
        int searchCloseButtonId = searchView.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButton = this.searchView.findViewById(searchCloseButtonId);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setQuery("", false);
                searchView.setIconified(true);
                setHairAndMakeupListAdapter();
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_search:
                // do something based on first item click
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
