package com.example.vipxy.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vipxy.R;
import com.example.vipxy.activity.adapter.OrderedSummaryAdapter;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderedSummaryActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.separator)
    View separator;
    @BindView(R.id.separator_1)
    View separator1;
    @BindView(R.id.btn_checkout)
    Button btnCheckout;
    @BindView(R.id.bottomsheet)
    LinearLayout bottomsheet;
    BottomSheetBehavior mBottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordered_summary);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setBottomsheet();
    }

    private void setBottomsheet() {
        bottomsheet = findViewById(R.id.bottomsheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomsheet);
        bottomsheet.setMinimumHeight(1);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        setOrderedListAdapter();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }

    private void setOrderedListAdapter() {
        ArrayList<Integer> imageArray = new ArrayList<>();
        imageArray.add(R.drawable.bespoke_blow_dry);
        imageArray.add(R.drawable.sports_massage);
        imageArray.add(R.drawable.express_mani_pedi);


        ArrayList<String> headingArray = new ArrayList<>();
        headingArray.add("BESPOKE BLOW DRY");
        headingArray.add("SPORTS MASSAGE");
        headingArray.add("BESPOKE MAKEUP");


        ArrayList<String> priceArray = new ArrayList<>();
        priceArray.add("$99");
        priceArray.add("$99");
        priceArray.add("$135");

        ArrayList<String> detailArray = new ArrayList<>();
        detailArray.add("Beauty|Short overview or specification of this service will be here Beauty");
        detailArray.add("Beauty|Short overview or specification of this service will be here");
        detailArray.add("Beauty|Short overview or specification of this service will be here");


        OrderedSummaryAdapter orderedSummaryAdapter = new OrderedSummaryAdapter(this, imageArray, headingArray, priceArray, detailArray);
        recyclerview.setLayoutManager(new LinearLayoutManager(OrderedSummaryActivity.this));
        recyclerview.setHasFixedSize(false);
        recyclerview.setAdapter(orderedSummaryAdapter);

    }

}
