package com.example.vipxy.activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.vipxy.R;
import com.example.vipxy.activity.adapter.HomeAdapter;

import com.example.vipxy.activity.interfaces.CommonInterface;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class HomeActivity extends AppCompatActivity implements CommonInterface {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.searchView)
    SearchView searchView;

    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES = {R.drawable.hair_and_makeup, R.drawable.nails, R.drawable.hair_and_makeup, R.drawable.acrylic_manicure};
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    LinearLayoutManager linearLayoutManager;
    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);


        toolbar.setNavigationIcon(R.drawable.back_button);

        toolbar.setSubtitle("Back");
        toolbar.setNavigationContentDescription("Back");
        setSupportActionBar(toolbar);
        init();
        setListAdapter();

    }



    private void setListAdapter() {
        HomeAdapter homeAdapter = new HomeAdapter(HomeActivity.this);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setHasFixedSize(false);
        recyclerview.setAdapter(homeAdapter);

    }

    private void init() {
        for (int i = 0; i < IMAGES.length; i++)
            ImagesArray.add(IMAGES[i]);

        mPager = (ViewPager) findViewById(R.id.pager);


        mPager.setAdapter(new com.example.vipxy.activity.SlidingImage_Adapter(HomeActivity.this, ImagesArray));
        indicator.createIndicators(5, 0);


        indicator.setViewPager(mPager);


        NUM_PAGES = IMAGES.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
                indicator.animatePageSelected(currentPage);
            }
        };
    /*    Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);*/

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }


}
