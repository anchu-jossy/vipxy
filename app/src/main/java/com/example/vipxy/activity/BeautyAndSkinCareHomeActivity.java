package com.example.vipxy.activity;

import android.os.Bundle;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vipxy.R;
import com.example.vipxy.activity.adapter.BeautyAndSkinCareHomeAdapter;


import butterknife.BindView;
import butterknife.ButterKnife;

public class BeautyAndSkinCareHomeActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beauty_home);
        ButterKnife.bind(this);
        setListAdapter();
    }

    private void setListAdapter() {

        BeautyAndSkinCareHomeAdapter homeAdapter = new BeautyAndSkinCareHomeAdapter(BeautyAndSkinCareHomeActivity.this);
        recyclerview.setLayoutManager(new GridLayoutManager(BeautyAndSkinCareHomeActivity.this,3));
        recyclerview.setHasFixedSize(false);
        recyclerview.setAdapter(homeAdapter);

    }
}
