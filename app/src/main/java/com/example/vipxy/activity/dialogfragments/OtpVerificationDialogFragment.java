package com.example.vipxy.activity.dialogfragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.vipxy.R;

public class OtpVerificationDialogFragment extends androidx.fragment.app.DialogFragment{




        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.otp_dialog, container, false);

        }

        @Override
        public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);


            /*final EditText editText = view.findViewById(R.id.inEmail);

            if (getArguments() != null && !TextUtils.isEmpty(getArguments().getString("email")))
                editText.setText(getArguments().getString("email"));

            Button btnDone = view.findViewById(R.id.btnDone);
            btnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DialogListener dialogListener = (DialogListener) getActivity();
                    dialogListener.onFinishEditDialog(editText.getText().toString());
                    dismiss();
                }
            });*/
        }

        @Override
        public void onResume() {
            super.onResume();

        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            /*Log.d("API123", "onCreate");

            boolean setFullScreen = false;
            if (getArguments() != null) {
                setFullScreen = getArguments().getBoolean("fullScreen");
            }

            if (setFullScreen)
                setStyle(OtpVerificationDialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);*/
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
        }

public interface DialogListener {
    void onFinishEditDialog(String inputText);
}
}