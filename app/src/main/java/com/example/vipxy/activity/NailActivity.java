package com.example.vipxy.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.PagerTabStrip;
import androidx.viewpager.widget.ViewPager;

import com.example.vipxy.R;
import com.example.vipxy.activity.adapter.NailsViewPagerAdapter;
import com.example.vipxy.activity.fragments.ManiPedisFragment;
import com.example.vipxy.activity.fragments.ManicureFragment;
import com.example.vipxy.activity.fragments.PedicureFragment;
import com.example.vipxy.activity.interfaces.CommonInterface;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.bottomsheet)
    LinearLayout bottomsheet;
    BottomSheetBehavior mBottomSheetBehavior;

    /*   @BindView(R.id.tabs)
       TabLayout tabs;*/
    @BindView(R.id.viewpager)
    ViewPager viewpager;

    ArrayList<String> fragmentArray = new ArrayList<>();
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.ll_vertical)
    LinearLayout llVertical;
    @BindView(R.id.pager_tab_strip)
    PagerTabStrip pagerTabStrip;
    SearchView searchView;
    String TAG = "NailActivity123";
    Context context;
    ManicureFragment manicureFragment = new ManicureFragment(NailActivity.this);
    NailActivity activity;
    CommonInterface commonInterface = null;
    @BindView(R.id.toolbar_back_btn)
    ImageView toolbarBackBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nails);
        ButterKnife.bind(this);

        tvToolbarTitle.setText(R.string.nails);
        setSupportActionBar(toolbar);
        setBottomsheet();
        addTabs(viewpager);


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return false;
    }


    private void setBottomsheet() {
        bottomsheet = findViewById(R.id.bottomsheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomsheet);
        bottomsheet.setMinimumHeight(1);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

      /*  searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d(TAG, "onQueryTextSubmit: "+s);
                if (viewpager.getCurrentItem() == 0) {


                }


                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                //  manicureFragment.onCloseClick();
                return true;
            }
        });*/
        return false;
    }


    private void addTabs(ViewPager viewPager) {


        NailsViewPagerAdapter adapter = new NailsViewPagerAdapter(getSupportFragmentManager(), fragmentArray);
        adapter.addFrag(new ManicureFragment(NailActivity.this), "Manicures");
        adapter.addFrag(new PedicureFragment(), "Pedicure");
        adapter.addFrag(new ManiPedisFragment(), "Mani-padis");

        viewPager.setAdapter(adapter);
    }


    @OnClick(R.id.toolbar_back_btn)
    public void onViewClicked() {
        onBackPressed();
    }
}
