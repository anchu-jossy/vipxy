package com.example.vipxy.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vipxy.R;
import com.example.vipxy.activity.adapter.Express_Mani_pedi_SubActivity_Adapter;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Expresss_Mani_Pedi_SubActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.tv_heading)
    TextView tvHeading;
    @BindView(R.id.bottomsheet)
    LinearLayout bottomsheet;
BottomSheetBehavior mBottomSheetBehavior;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mani__pedi__sub);
        ButterKnife.bind(this);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomsheet);
        bottomsheet.setMinimumHeight(1);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void setListAdapter() {
        ArrayList<String> headingArray = new ArrayList<>();
        headingArray.add("Acrylic Removal");
        headingArray.add("Gel Removal");
        headingArray.add("Gel Application");
        headingArray.add("French Finish");
        headingArray.add("Foot Soak");
        headingArray.add("Extra Massage");
        headingArray.add("Nail Art");


        ArrayList<String> priceArray = new ArrayList<>();
        priceArray.add("$30 for 30 minutes");
        priceArray.add("$10 for 30 minutes");
        priceArray.add("$15 for 05 minutes");
        priceArray.add("$10 for 15 minutes");
        priceArray.add("$10 for 10 minutes");
        priceArray.add("$10 for 10 minutes");
        priceArray.add("(1 design on 2 fingers)$5 for 10 minutes");
        ArrayList<Integer> imageArray = new ArrayList<>();
        imageArray.add(R.drawable.menu_beauty);
        imageArray.add(R.drawable.menu_home_care);
        imageArray.add(R.drawable.menu_health);
        Express_Mani_pedi_SubActivity_Adapter adapter = new Express_Mani_pedi_SubActivity_Adapter(Expresss_Mani_Pedi_SubActivity.this, headingArray, priceArray);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setHasFixedSize(false);
        recyclerview.setAdapter(adapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        setListAdapter();
    }
}
