package com.example.vipxy.activity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vipxy.R;
import com.example.vipxy.activity.Expresss_Mani_Pedi_SubActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Express_Mani_pedi_SubActivity_Adapter extends RecyclerView.Adapter<Express_Mani_pedi_SubActivity_Adapter.MyViewHolder> {


    ArrayList<String> headingArray = new ArrayList<>();
    ArrayList<String> priceArray = new ArrayList<>();

    Expresss_Mani_Pedi_SubActivity context;



    public Express_Mani_pedi_SubActivity_Adapter(Expresss_Mani_Pedi_SubActivity mani_pedi_subActivity, ArrayList<String> headingArray, ArrayList<String> priceArray) {
        this.context = mani_pedi_subActivity;

        this.headingArray = headingArray;
        this.priceArray = priceArray;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_manipedi_subactivity, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.tvHeading.setText(headingArray.get(position));
        holder.tvPrice.setText(priceArray.get(position));
        holder.tvHeading.setText(headingArray.get(position));

    }


    @Override
    public int getItemCount() {
        return headingArray.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_heading)
        TextView tvHeading;
        @BindView(R.id.tv_price)
        TextView tvPrice;

        @BindView(R.id.separator)
        View separator;
        @BindView(R.id.card)
        LinearLayout card;
        @BindView(R.id.checkBox)
        CheckBox checkBox;
        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }
}
