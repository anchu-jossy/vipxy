package com.example.vipxy.activity.dialogfragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.vipxy.R;
import com.example.vipxy.activity.OrderedSummaryActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class PackageSelectionDialogFragment extends DialogFragment {


    @BindView(R.id.radioButton1)
    RadioButton radioButton1;
    @BindView(R.id.radioButton2)
    RadioButton radioButton2;
    @BindView(R.id.radioButton3)
    RadioButton radioButton3;
    @BindView(R.id.radioPackage)
    RadioGroup radioPackage;
    @BindView(R.id.radioButton_male)
    RadioButton radioButtonMale;
    @BindView(R.id.radioButton_female)
    RadioButton radioButtonFemale;
    @BindView(R.id.radioButton3_either)
    RadioButton radioButton3Either;
    @BindView(R.id.radiogender)
    RadioGroup radiogender;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
   String TAG="PackageSelectionDialog";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.package_selection_dialog, container, false);

    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);


        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setGravity(Gravity.CENTER);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btnDone = view.findViewById(R.id.btn_submit);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onViewClicked: ");
                startActivity(new Intent(getActivity(), OrderedSummaryActivity.class));
            }
        });
        //     setStyle(PackageSelectionDialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);

            /*final EditText editText = view.findViewById(R.id.inEmail);

            if (getArguments() != null && !TextUtils.isEmpty(getArguments().getString("email")))
                editText.setText(getArguments().getString("email"));


    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            /*Log.d("API123", "onCreate");

            boolean setFullScreen = false;
            if (getArguments() != null) {
                setFullScreen = getArguments().getBoolean("fullScreen");
            }

            if (setFullScreen)
                setStyle(OtpVerificationDialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);*/
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }



    public interface DialogListener {
        void onFinishEditDialog(String inputText);
    }
}