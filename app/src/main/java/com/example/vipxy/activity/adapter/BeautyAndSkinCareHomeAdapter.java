package com.example.vipxy.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vipxy.R;
import com.example.vipxy.activity.BeautyAndSkinCareHomeActivity;

public class BeautyAndSkinCareHomeAdapter extends RecyclerView.Adapter<BeautyAndSkinCareHomeAdapter.MyViewHolder> {
    Context context;
    ImageView imageView;
    public BeautyAndSkinCareHomeAdapter(BeautyAndSkinCareHomeActivity beautyHomeActivity) {
        this.context=beautyHomeActivity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_beauty_home_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
    }


    @Override
    public int getItemCount() {
        return 15;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public MyViewHolder(View view) {
            super(view);


        }
    }
}
