package com.example.vipxy.activity;

import android.app.SearchManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vipxy.R;
import com.example.vipxy.activity.adapter.MassageHealthAdapter;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MassageHealthActivity extends AppCompatActivity {

    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.bottomsheet)
    LinearLayout bottomsheet;
    BottomSheetBehavior mBottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_massage);
        ButterKnife.bind(this);
        tvToolbarTitle.setText(R.string.massage);
        setSupportActionBar(toolbar);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomsheet);
        bottomsheet.setMinimumHeight(1);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        setHairAndMakeupListAdapter();
    }

    private void setHairAndMakeupListAdapter() {
        ArrayList<Integer> imageArray = new ArrayList<>();
        imageArray.add(R.drawable.swedish_relaxation_massage);
        imageArray.add(R.drawable.sports_massage);
        imageArray.add(R.drawable.pregnancy_massage);
        imageArray.add(R.drawable.couples_massage);
        imageArray.add(R.drawable.remedial_massage);
        imageArray.add(R.drawable.corporate_chair_massage);

        ArrayList<String> headingArray = new ArrayList<>();
        headingArray.add("SWEDISH RELAXATION MASSAGE");
        headingArray.add("SPORTS MASSAGE");
        headingArray.add("PREGNANCY MASSAGE");
        headingArray.add("COUPLES MASSAGE");
        headingArray.add("REMEDIAL MASSAGE");
        headingArray.add("CORPORATE CHAIR MASSAGE");




        ArrayList<String> price1Array = new ArrayList<>();
        price1Array.add("$99 for 60 minutes");
        price1Array.add("$99 for 60 minutes");
        price1Array.add("$99 for 60 minutes");
        price1Array.add("$99 for 60 minutes");
        price1Array.add("$99 for 60 minutes");
        price1Array.add("$99 for 60 minutes");

        ArrayList<String> price2Array = new ArrayList<>();
        price2Array.add("$139 for 90 minutes");
        price2Array.add("$139 for 90 minutes");
        price2Array.add("$139 for 90 minutes");
        price2Array.add("$139 for 90 minutes");
        price2Array.add("$139 for 90 minutes");
        price2Array.add("$139 for 90 minutes");

        ArrayList<String> price3Array = new ArrayList<>();
        price3Array.add("$179 for 120 minutes");
        price3Array.add("$179 for 120 minutes");
        price3Array.add("$179 for 120 minutes");
        price3Array.add("$179 for 120 minutes");
        price3Array.add("$179 for 120 minutes");
        price3Array.add("$179 for 120 minutes");



        MassageHealthAdapter homeAdapter = new MassageHealthAdapter(this, imageArray, headingArray, price1Array, price2Array,price3Array);
        recyclerview.setLayoutManager(new LinearLayoutManager(MassageHealthActivity.this));
        recyclerview.setHasFixedSize(false);
        recyclerview.setAdapter(homeAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }
}

