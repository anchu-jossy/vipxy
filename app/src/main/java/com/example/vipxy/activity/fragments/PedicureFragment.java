package com.example.vipxy.activity.fragments;


import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.SearchView;

import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vipxy.R;
import com.example.vipxy.activity.adapter.PedicureAdapter;

import java.util.ArrayList;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.SEARCH_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class PedicureFragment extends Fragment {

    ArrayList<Integer> imageArray = new ArrayList<>();
    ArrayList<String> headingArray = new ArrayList<>();
    ArrayList<String> priceArray = new ArrayList<>();
    ArrayList<String> detailArray = new ArrayList<>();
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    SearchView searchView;
    PedicureAdapter adapter;
    private String TAG = "PedicureFragment123";

    public PedicureFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageArray.add(R.drawable.classic_pedicure);
        imageArray.add(R.drawable.luxury_pedicure);
        imageArray.add(R.drawable.gel_menucure);

        headingArray.add("CLASSIC PEDICURE");
        headingArray.add("LUXURY PEDICURE");
        headingArray.add("GEL PEDICURE");

        priceArray.add("$85 for 45 minutes");
        priceArray.add("$99 for 60 minutes");
        priceArray.add("$100 for 45 minutes");

        detailArray.add("A perfect treatment for those who want a pampering for feet and nails!");
        detailArray.add("A perfect treatment for those who want a pampering for feet and nails!");
        detailArray.add("Beauty|Short overview or specification of this service will be here");

    }

    private void hideKeyboard() {
        View view = this.getView();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);

        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getContext().getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d(TAG, "onQueryTextSubmit: " + s);

                passDataFromToolbar(s);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });


        int searchCloseButtonId = searchView.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButton = (ImageView) this.searchView.findViewById(searchCloseButtonId);
// Set on click listener
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setQuery("", false);
                settingAdapter();
                searchView.setIconified(true);
                hideKeyboard();
                Log.d(TAG, "onClose: &&&&&");
            }
        });
    }

    public void passDataFromToolbar(String s) {
        String newText = s.toLowerCase();

        for (int i = 0; i < headingArray.size(); i++) {


            if (Pattern.compile(Pattern.quote(headingArray.get(i)), Pattern.CASE_INSENSITIVE).matcher(newText).find()) {
                Log.d(TAG, "onQueryTextSubmit: " + newText + headingArray.get(i));
                ArrayList<String> heading1Array = new ArrayList<>();
                heading1Array.add(headingArray.get(i));
                ArrayList<String> details1Array = new ArrayList<>();
                details1Array.add(detailArray.get(i));
                ArrayList<Integer> image1Array = new ArrayList<>();
                image1Array.add(imageArray.get(i));
                ArrayList<String> price1Array = new ArrayList<>();
                price1Array.add(priceArray.get(i));
                adapter.notifyDataSetChanged();
                //  homeAdapter.setFilter(headingArray, DetailsArray, imageArray);
                adapter = new PedicureAdapter(this, image1Array, heading1Array, price1Array, details1Array);

                recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerview.setHasFixedSize(false);
                recyclerview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                Log.d(TAG, "onQueryTextSubmit:nomatch ");
            }


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pedicure, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onStart() {

        super.onStart();
        settingAdapter();
    }

    private void settingAdapter() {
        adapter = new PedicureAdapter(this, imageArray, headingArray, priceArray, detailArray);
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerview.setAdapter(adapter);
    }
}
